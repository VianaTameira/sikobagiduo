import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import javax.sound.sampled.*;
public class cuba {
    public static void main(String[] args) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Kamu sedang berjalan, ada yang menepok bahumu dari belakang dan berkata \"kak barangnya jatoh\"");
        System.out.println("Bagaimana tanggapanmu: ");
        System.out.println("A. Berterimakasih");
        System.out.println("B. Berkata \"Oh ini lagi dengerin lagu siko bagi duo\"");

        File file = new File("SikokBagiDuo.wav");
        AudioInputStream audiostream = AudioSystem.getAudioInputStream(file);
        Clip clip = AudioSystem.getClip();
        clip.open(audiostream);

        String tanggapan = "";

        while (!tanggapan.equals("A")) {
            System.out.println("Masukkan tanggapanmu (A/B): ");
            tanggapan = scanner.next();
            tanggapan = tanggapan.toUpperCase();

            switch (tanggapan) {
                case ("B"):
                    clip.start();
                    break;
                case ("A"):
                    System.out.println("Okee byee!!");
                    break;
                default:
                    System.out.println("Tanggapan yang dimasukkan tidak valid");
            }
        }
        System.out.println("Byeeee!!!");


    }
}